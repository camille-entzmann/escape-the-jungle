# Technologies utilisées

## QR Code

Génération de code-barre 2D (QR Code) avec l'outils [en ligne zxing](http://zxing.appspot.com/generator)

![](qrcode_object2.png)

## Site statique

Utilisation du générateur de site statique [Hugo](https://gohugo.io/) et du thème [blowfish](https://blowfish.page/)

## Gitlab

Gestion de projet et publication de site statique.

## Canva

Design collaboratif.